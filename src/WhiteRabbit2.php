<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        /*
		This function is not so simple to explain by comments
		since it uses dynamic programming algorithm.
		The idea here is not to use greedy algorithm because it's not
		always correct (at least not for all sets of denominations).
		The dynamic programming is specific in case we need to use the values
		we already calculated. We store such values and use them when it's needed.
		That way we decrease the complexity of an algorithm.
		*/
		
		$denominations = array(1,2,5,10,20,50,100);
		$numberOfCoins = array(0,0,0,0,0,0,0);
		
		$minCoins = array_fill(1, $amount, PHP_INT_MAX);
		$minCoins[0] = 0;
		
		$coinIndex = array_fill(0, $amount + 1, -1);
		$coinIndex[0] = 0;
		
		for($i = 1; $i <= $amount; $i++){
			for($j = 0; $j < sizeof($denominations); $j++){
				if($denominations[$j]<=$i){
					if($minCoins[$i - $denominations[$j]] +1 < $minCoins[$i]) {
						$minCoins[$i] = 1+$minCoins[$i - $denominations[$j]];
						$coinIndex[$i] = $j;
					}
				}
				
			}
		}
		
		while($amount > 0){
			$numberOfCoins[$coinIndex[$amount]]++;
			$amount -= $denominations[$coinIndex[$amount]];
		}
		
        
        return array(
                '1'   => $numberOfCoins[0],
                '2'   => $numberOfCoins[1],
                '5'   => $numberOfCoins[2],
                '10'  => $numberOfCoins[3],
                '20'  => $numberOfCoins[4],
                '50'  => $numberOfCoins[5],
                '100' => $numberOfCoins[6]
        );
    }
}