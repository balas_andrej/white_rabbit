<?php
mb_internal_encoding( 'UTF-8' );

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
		$allChars = array();
        $file = file_get_contents($filePath); //store whole text file as a string into $file variable
		
		$chars = preg_split('/(?<!^)(?!$)/u', $file ); //separating all characters into $chars variable

		foreach ($chars as $c) {			//for each character
			if (isset($allChars[$c])) {		//check if it already has value in the table
				$allChars[$c]++;			//if yes then increase counter
			}								//else check if it is a (Basic Latin or Latin-1 Supplement) letter and declare the counter
			else {
				if(preg_match('/[\x{0041}-\x{005A}\x{0061}-\x{007A}\x{00C0}-\x{00D6}\x{00D8}-\x{00F6}\x{00F8}-\x{00FF}]/u', $c)) {
					$allChars[$c] = 1;
				}
			}
		}
		
		foreach($allChars as $key => $value){
			if($key >= 'A' && $key <= 'Z'){						//if the letter is between Basic Latin count all as lowercase
				if(isset($allChars[chr(ord($key)+32)])){
					$allChars[chr(ord($key)+32)] += $value;
				}
				else {
					$allChars[chr(ord($key)+32)] = $value;
				}
				unset($allChars[$key]);
			}
		}
		
		return $allChars;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
		asort($parsedFile);
		$letterKeys = array_keys($parsedFile);						//put all the letters into separate array
		$letterValues = array_values($parsedFile);					//put all the occurrences into separate array as well
		$occurrences = $letterValues[floor(count($parsedFile)/2)];	//set $occurrences variable with the array mean
		return $letterKeys[floor(count($parsedFile)/2)];			//return the mean letter
    }
}